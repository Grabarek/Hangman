import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: () => ({
            counter: 0,
            word: '',
            splittedWord: [],
            showStart: true,
            started: false,
            ended: false,
            success: false,
            step: 0,
            missedLetters: [],
            maxTries: 11,
            occurences: [],
            hintText: '',
            hintShow: false,
            infoText: 'Vue Hangman'
        }),
        mutations: {
            start(state) {
                state.showStart = false;
                state.ended = false;
                state.success = false;
                state.started = true;
                state.step = 0;
                state.occurences = [];
                state.missedLetters = [];
            },
            fail(state) {
                state.ended = true;
                state.occurences = [],
                state.infoText = 'The word was: "'+state.word+'". Better luck next time!'
            },
            success(state) {
                state.success = true;
                state.infoText = 'Good job!';
            },
            setWord(state, randomWord) {
                state.word = randomWord;
            },
            splitWord(state, splittedWord) {
                state.splittedWord = splittedWord;
            },
            nextStep(state) {
                state.step++;
            },
            resetStep(state) {
                state.step = 0;
            },
            addMissedLetter(state, letter){
                state.missedLetters.push(letter);
            },
            setOccurence(state, occurence) {
                for (var i in occurence) {
                    if(state.occurences.includes(occurence[i])) {
                        return false;
                    } else {
                        state.occurences.push(occurence[i]);
                    }
                }
            },
            toggleHint(state, hint) {
                state.hintShow = false;
                state.hintText = hint;
                state.hintShow = true;
                let timer = setTimeout(function(){
                    state.hintShow = false;
                }, 3200)
            }
        },
        actions: {
            start(app) {
                let randomWords = require('random-words');
                let randomWord = randomWords();
                let splittedWord = randomWord.split('');
                app.commit('start');
                app.commit('setWord', randomWord);
                app.commit('splitWord', splittedWord);
            },
            fail(app) {
                app.commit('fail');
            },
            success(app) {
                app.commit('success');
            },
            nextStep(step) {
                step.commit('nextStep');
            },
            resetStep(step) {
                step.commit('resetStep');
            },
            setOccurence(occurence) {
                occurence.commit('setOccurence');
            }
        }
    })
}

export default createStore
